Step-by-Step Tasks and Scripts:
Basic Commands:
Objective: Familiarize with the Linux command line and basic file manipulation.
Tasks and Script:
bash
Copy code
#!/bin/bash
# Navigate and display current directory
echo "Navigating the filesystem:"
pwd
ls -la
cd /var/www
echo "Changed directory to $(pwd)"

# Create, delete, and manage directories
echo "Creating directories..."
mkdir -p /var/www/html
echo "Directories created:"
ls -la /var/www

# Managing files
echo "Creating a sample file..."
echo "Hello World" > /var/www/html/index.html
cat /var/www/html/index.html
Linux Users & Groups Commands:
Objective: Understand how to manage users and groups on a Linux system.
Tasks and Script:
bash
Copy code
#!/bin/bash
# Create user and group for web server management
useradd -m -s /bin/bash webadmin
groupadd webgroup
usermod -aG webgroup webadmin
echo "User and group created for web management"
Linux Permissions and Files and Directories:
Objective: Learn about Linux file permissions and how to manage them.
Tasks and Script:
bash
Copy code
#!/bin/bash
# Set directory permissions
chown -R webadmin:webgroup /var/www/html
chmod -R 755 /var/www/html
echo "Permissions set for web directories"
Linux Package Management:
Objective: Master the installation and management of software packages.
Tasks and Script:
bash
Copy code
#!/bin/bash
# Update and install Nginx
apt-get update
apt-get install -y nginx
echo "Nginx installed successfully"
Environment Variables and Bash Scripting:
Objective: Understand environment variables and basic bash scripting for automation.
Tasks and Script:
bash
Copy code
#!/bin/bash
# Define and export environment variables
export WEB_DIR=/var/www/html
export BACKUP_DIR=/var/backup

# Backup script using environment variables
mkdir -p $BACKUP_DIR
cp -R $WEB_DIR/* $BACKUP_DIR/
echo "Backup completed to $BACKUP_DIR"
Text Processing:
Objective: Manipulate and process text files using command-line tools.
Tasks and Script:
bash
Copy code
#!/bin/bash
# Analyze and extract data from web server logs
grep "404" /var/log/nginx/access.log > /tmp/404_errors.txt
echo "404 errors extracted to /tmp/404_errors.txt"
systemd:
Objective: Learn to manage system services and processes with systemd.
Tasks and Script:
bash
Copy code
#!/bin/bash
# Manage Nginx service
systemctl start nginx
systemctl enable nginx
systemctl status nginx | grep "active"