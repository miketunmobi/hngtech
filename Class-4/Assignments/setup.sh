#!/bin/bash

# Enhance output readability
echo "======================="
echo "Starting Docker setup..."
echo "======================="

# Check if Docker is already installed
if [ -x "$(command -v docker)" ]; then
    echo "Error: Docker is already installed on this system."
    echo "Please remove the existing installation before running this setup script."
    exit 1
fi

# Checking current working directory and changing if not as expected
echo "Current working directory: $(pwd)"
cd /tmp
echo "Changed to temporary directory: $(pwd)"

# Create a directory specific for this operation to keep things tidy
setup_dir="docker_setup"
mkdir -p $setup_dir
cd $setup_dir

# Update package list and upgrade packages
echo "Updating package list and upgrading existing packages..."
sudo apt-get update && sudo apt-get upgrade -y

# Install necessary packages for Docker installation
echo "Installing required packages for Docker..."
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# Add Docker’s official GPG key:
echo "Adding Docker's official GPG key..."
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add Docker repository
echo "Adding Docker repository to APT sources..."
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Update package list after adding Docker's repository
echo "Updating package list..."
sudo apt-get update

# Install Docker
echo "Installing Docker CE..."
sudo apt-get install -y docker-ce

# Adding a new user for Docker management
new_user="dockeradmin"
echo "Creating a new user: $new_user"
sudo adduser --disabled-password --gecos "" $new_user

# Add the new user to the Docker group
echo "Adding $new_user to the Docker group..."
sudo usermod -aG docker $new_user

# Create a directory for Docker volumes
directory="/var/lib/docker-volumes"
echo "Creating directory for Docker volumes: $directory"
sudo mkdir -p $directory
sudo chown $new_user:docker $directory
sudo chmod 770 $directory

# Display Docker version
echo "Docker version:"
sudo docker version

# Check Docker status
echo "Checking Docker service status..."
sudo systemctl start docker
sudo systemctl status docker | grep "Active"

# Set environment variable for script completion
export SETUP_COMPLETE=true
echo "SETUP_COMPLETE set to $SETUP_COMPLETE"
if [ "$SETUP_COMPLETE" = true ]; then
    echo "Setup process completed successfully."
fi

# List all files and directories in the current directory
echo "Listing all items in directory:"
ls -l

# Example of cleanup operation
echo "Cleaning up setup files..."
cd ..
sudo rm -rf $setup_dir  # Removing only the directory created for setup

# Completion message
echo "========================"
echo "Docker setup completed!"
echo "========================"
